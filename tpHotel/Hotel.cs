﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        private int id;
        private string nom;
        private string adresse;
        private string ville;
        public Hotel(int unId, string unNom, string uneAdresse, string uneVille)
        {
            id = unId;
            nom = unNom;
            adresse = uneAdresse;
            ville = uneVille;
        }
        

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
    class Chambre
    {
        private int num;
        private int id;
        private int etage;
        private string Description;
        
        public Chambre(int unNum,int unId, int unEtage, string uneDescription)
        {
            Num = unNum;
            Id = unId;
            Description1 = uneDescription;
            etage = unEtage;
        }

        public int Num { get => num; set => num = value; }
        public int Id { get => id; set => id = value; }
        public string Description1 { get => Description; set => Description = value; }
        public int Etage { get => etage; set => etage = value; }
    }
}
