﻿namespace tpHotel
{
    partial class frmAccueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAjoutHotel = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.btnVoirHotel = new System.Windows.Forms.Button();
            this.btnAjoutChambre = new System.Windows.Forms.Button();
            this.btnVoirChbr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAjoutHotel
            // 
            this.btnAjoutHotel.Location = new System.Drawing.Point(83, 55);
            this.btnAjoutHotel.Margin = new System.Windows.Forms.Padding(4);
            this.btnAjoutHotel.Name = "btnAjoutHotel";
            this.btnAjoutHotel.Size = new System.Drawing.Size(193, 43);
            this.btnAjoutHotel.TabIndex = 0;
            this.btnAjoutHotel.Text = "Ajout hôtels";
            this.btnAjoutHotel.UseVisualStyleBackColor = true;
            this.btnAjoutHotel.Click += new System.EventHandler(this.btnAjoutHotel_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Location = new System.Drawing.Point(227, 313);
            this.btnQuitter.Margin = new System.Windows.Forms.Padding(4);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(105, 37);
            this.btnQuitter.TabIndex = 1;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // btnVoirHotel
            // 
            this.btnVoirHotel.Location = new System.Drawing.Point(83, 106);
            this.btnVoirHotel.Margin = new System.Windows.Forms.Padding(4);
            this.btnVoirHotel.Name = "btnVoirHotel";
            this.btnVoirHotel.Size = new System.Drawing.Size(193, 43);
            this.btnVoirHotel.TabIndex = 1;
            this.btnVoirHotel.Text = "Visualiser hôtels";
            this.btnVoirHotel.UseVisualStyleBackColor = true;
            this.btnVoirHotel.Click += new System.EventHandler(this.btnVoirHotel_Click);
            // 
            // btnAjoutChambre
            // 
            this.btnAjoutChambre.Location = new System.Drawing.Point(83, 161);
            this.btnAjoutChambre.Name = "btnAjoutChambre";
            this.btnAjoutChambre.Size = new System.Drawing.Size(192, 47);
            this.btnAjoutChambre.TabIndex = 2;
            this.btnAjoutChambre.Text = "Ajout chambre";
            this.btnAjoutChambre.UseVisualStyleBackColor = true;
            this.btnAjoutChambre.Click += new System.EventHandler(this.btnAjoutChambre_Click);
            // 
            // btnVoirChbr
            // 
            this.btnVoirChbr.Location = new System.Drawing.Point(83, 226);
            this.btnVoirChbr.Name = "btnVoirChbr";
            this.btnVoirChbr.Size = new System.Drawing.Size(191, 47);
            this.btnVoirChbr.TabIndex = 3;
            this.btnVoirChbr.Text = "Visualiser chambres";
            this.btnVoirChbr.UseVisualStyleBackColor = true;
            this.btnVoirChbr.Click += new System.EventHandler(this.btnVoirChbr_Click);
            // 
            // frmAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 388);
            this.Controls.Add(this.btnVoirChbr);
            this.Controls.Add(this.btnAjoutChambre);
            this.Controls.Add(this.btnVoirHotel);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnAjoutHotel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmAccueil";
            this.Text = "Accueil";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAjoutHotel;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Button btnVoirHotel;
        private System.Windows.Forms.Button btnAjoutChambre;
        private System.Windows.Forms.Button btnVoirChbr;
    }
}