﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();

            foreach (Hotel item in Persistance.getLesHotels())
            {

                cboxHotel.Items.Add(item.Nom);
            }

        }



        private void btnEnregistrer_Click(object sender, EventArgs e)
        {

            Persistance.ajouteChambre(cboxHotel.Text, UpDownEtage.Value, txtboxDescr.Text);
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            UpDownEtage.Value = 0;
            txtboxDescr.Text = "";
            cboxHotel.Text = "";
        }


    }
}
