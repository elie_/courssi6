﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmVoirChambre : Form
    {
        public frmVoirChambre()
        {
            InitializeComponent();
            // lstChambre
            lstChambre.DataSource = Persistance.getLesChambres();
        }



        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
