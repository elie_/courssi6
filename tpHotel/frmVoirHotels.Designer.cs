﻿namespace tpHotel
{
    partial class frmVoirHotels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFermer = new System.Windows.Forms.Button();
            this.lstHotels = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.lstHotels)).BeginInit();
            this.SuspendLayout();
            // 
            // btnFermer
            // 
            this.btnFermer.Location = new System.Drawing.Point(421, 297);
            this.btnFermer.Margin = new System.Windows.Forms.Padding(4);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Size = new System.Drawing.Size(129, 49);
            this.btnFermer.TabIndex = 1;
            this.btnFermer.Text = "Fermer";
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            // 
            // lstHotels
            // 
            this.lstHotels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstHotels.Location = new System.Drawing.Point(48, 44);
            this.lstHotels.Margin = new System.Windows.Forms.Padding(4);
            this.lstHotels.Name = "lstHotels";
            this.lstHotels.Size = new System.Drawing.Size(461, 215);
            this.lstHotels.TabIndex = 2;
            this.lstHotels.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lstHotels_CellContentClick);
            // 
            // frmVoirHotels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 359);
            this.Controls.Add(this.lstHotels);
            this.Controls.Add(this.btnFermer);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmVoirHotels";
            this.Text = "frmVoirHotels";
            this.Load += new System.EventHandler(this.frmVoirHotels_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lstHotels)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnFermer;
        private System.Windows.Forms.DataGridView lstHotels;
    }
}